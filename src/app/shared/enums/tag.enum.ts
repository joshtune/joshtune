export enum Tag {
  Scrum = 'Scrum',

  Node = 'Node',
  TypeScript = 'Typescript',
  JavaScript = 'JavaScript',

  Git = 'Git',

  Angular = 'Angular',
  AngularJS = 'AngularJS',
  JQuery = 'JQuery',
  TaffyDB = 'taffyDB',
  RequireJS = 'RequireJS',
  JQueryWidgets = 'JQuery Widgets',
  WebPack = 'WebPack',

  Ruby = 'Ruby',
  Java = 'Java',
  PHP = 'PHP',
  DotNet = '.Net',
  CSharp = 'C#',
  Razor = 'Razor',
  Siverlight = 'Siverlight',

  Kubernetes = 'Kubernetes',
  TeamCity = 'Team City',
  Jenkins = 'Jenkins',

  SCSS = 'SCSS',
  LESS = 'LESS',
  CSS3 = 'CSS3',
  HTML5 = 'HTML5',
  ResponsiveWebDesign = 'Responsive Web Design',

  Docker = 'Docker',
  AWS = 'AWS',
  CMS = 'CMS',
  ClassicASP = 'Classic ASP',
  MySQL = 'MySQL',
  MariaDB = 'MariaDB',
  Postgres = 'Postgres',
  SQL = 'SQL',

  Umbracco = 'Umbracco',
  RedDot = 'RedDot',
  Drupal = 'Drupal',
  Wordpress = 'Wordpress',
  Velocity = 'Velocity'
}
