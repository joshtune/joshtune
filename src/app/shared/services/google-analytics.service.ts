import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';

export interface PageViewParameters {
  title: string;
  location: string;
  path: string;
}

export interface EventParameters {
  action: string;
  category: string;
  label: string;
  value?: number | string;
}

declare let gtag: (a, b, c?: any) => {};

@Injectable({
  providedIn: 'root'
})
export class GoogleAnalyticsService {

  enabled = false;

  constructor() {
    this.enabled = environment.production;
    if (this.enabled) {
      gtag('config', environment.googleAnalyticsTrackingID);
    }

  }

  eventEmitter(event: EventParameters): void {
    if (this.enabled) {
      gtag('event', event.action, {
        event_category: event.category,
        event_label: event.label,
        eventValue: event.value
      });
    }
  }

  pageView(event: PageViewParameters): void {
    if (this.enabled) {
      gtag('config', environment.googleAnalyticsTrackingID, {
        page_title: event.title,
        page_path: event.path,
        page_location: event.location
      });
    }
  }
}
