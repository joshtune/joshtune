import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NavigationComponent} from './components/navigation/navigation.component';
import {MeComponent} from './components/me/me.component';
import {ExperienceComponent} from './components/experience/experience.component';
import {ContactComponent} from './components/contact/contact.component';
import {ProfileComponent} from './components/profile/profile.component';
import {LogoComponent} from './components/logo/logo.component';
import {ScrollToModule} from '@nicky-lenaers/ngx-scroll-to';
import {StripesComponent} from './components/stripes/stripes.component';
import {MountainsComponent} from './components/mountains/mountains.component';
import {SunComponent} from './components/sun/sun.component';
import {ContactStripComponent} from './components/contact-strip/contact-strip.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {BgIslandComponent} from './components/bg-island/bg-island.component';
import {BgMountenousComponent} from './components/bg-mountenous/bg-mountenous.component';
import {BirdsComponent} from './components/birds/birds.component';
import { PortfolioComponent } from './components/portfolio/portfolio.component';

@NgModule({
  declarations: [
    // Content Components
    NavigationComponent,
    MeComponent,
    ProfileComponent,
    ExperienceComponent,
    PortfolioComponent,
    ContactComponent,

    // Stuff Components
    BirdsComponent,
    BgIslandComponent,
    BgMountenousComponent,
    ContactStripComponent,
    LogoComponent,
    MountainsComponent,
    StripesComponent,
    SunComponent,
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    ScrollToModule.forRoot()
  ],
  exports: [
    // Modules
    CommonModule,
    FontAwesomeModule,

    // Components
    NavigationComponent,
    MeComponent,
    ProfileComponent,
    ExperienceComponent,
    PortfolioComponent,
    ContactComponent,

    // Stuff Components
    BirdsComponent,
    BgIslandComponent,
    BgMountenousComponent,
    ContactStripComponent,
    LogoComponent,
    MountainsComponent,
    StripesComponent,
    SunComponent,

  ]
})
export class SharedModule {
}
