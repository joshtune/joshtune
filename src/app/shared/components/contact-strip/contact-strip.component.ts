import {Component, Input} from '@angular/core';
import {faEnvelope, faFilePdf, faMobileAlt} from '@fortawesome/free-solid-svg-icons';
import {faFacebook, faGithub, faGitlab, faLinkedin, faTwitter} from '@fortawesome/free-brands-svg-icons';
import {IconProp, SizeProp} from '@fortawesome/fontawesome-svg-core';
import {clone} from 'lodash/index';
import {GoogleAnalyticsService} from '../../services/google-analytics.service';

export interface ContactItem {
  name: string;
  label: string;
  href: string;
  icon: IconProp;
  link?: boolean;
}

const contactItems: ContactItem[] = [
  {
    // idea - https://developers.google.com/drive/api/v3/manage-downloads
    name: 'resume',
    label: 'Resume',
    href: '../../../assets/josh-tune-resume.pdf',
    icon: faFilePdf,
    link: true
  },
  {
    name: 'cell',
    label: 'Phone',
    href: 'tel:8018601598',
    icon: faMobileAlt,
    link: true
  },
  {
    name: 'email',
    label: 'Email',
    href: 'mailto:joshua.tune@gmail.com',
    icon: faEnvelope,
    link: true
  },
  {
    name: 'gitlab',
    label: 'GitLab',
    href: 'https://gitlab.com/joshtune',
    icon: faGitlab,
    link: true
  },
  {
    name: 'github',
    label: 'GitHub',
    href: 'https://github.com/joshtune',
    icon: faGithub,
    link: true
  },
  {
    name: 'linkedIn',
    label: 'LinkedIn',
    href: 'https://www.linkedin.com/in/joshtune/',
    icon: faLinkedin,
    link: true
  },
  {
    name: 'twitter',
    label: 'Twitter',
    href: 'https://twitter.com/joshtune',
    icon: faTwitter,
    link: true
  },
  {
    name: 'facebook',
    label: 'FaceBook',
    href: 'https://facebook.com/tunejosh',
    icon: faFacebook,
    link: true
  }
];

@Component({
  selector: 'app-contact-strip',
  templateUrl: './contact-strip.component.html',
  styleUrls: ['./contact-strip.component.scss']
})
export class ContactStripComponent {
  contactItems = contactItems;
  @Input() iconSize: SizeProp = '1x';
  buttonList: ContactItem[] = [];

  constructor(private googleAnalyticsService: GoogleAnalyticsService) {
    const list = clone(this.contactItems);
    this.buttonList = list.filter(i => i.link);
  }

  itemClicked(item: ContactItem): void {
    this.googleAnalyticsService.eventEmitter({
      action: `Clicked on ${item.name}`,
      label: `contact_strip`,
      category: `contact_me`
    });
  }
}
