import { Component } from '@angular/core';
import {ContactItem} from '../contact-strip/contact-strip.component';
import {GoogleAnalyticsService} from '../../services/google-analytics.service';
import {clone} from 'lodash/index';

@Component({
  selector: 'app-me',
  templateUrl: './me.component.html',
  styleUrls: ['./me.component.scss']
})
export class MeComponent {
  constructor(private googleAnalyticsService: GoogleAnalyticsService) {}

  hireMeClick(): void {
    this.googleAnalyticsService.eventEmitter({
      action: `Clicked on Hire Me Button`,
      label: `me_button`,
      category: `contact_me`
    });
  }
}
