import {Component} from '@angular/core';
import {Tag} from '../../enums/tag.enum';

export interface Experience {
  title: string;
  company: string;
  location: string;
  start: string;
  end: string;
  descriptions: string[];
  tags: Tag[];
}

const experiences: Experience[] = [
  {
    title: 'Senior Software Engineer',
    company: 'Imagine Learning',
    location: 'Provo, UT',
    start: 'Nov 2014',
    end: 'Nov 2019',
    descriptions: [
      `Lead & Mentored a team of five in developing
        Imagine Math Educator Dashboard, separating
        front-end from monolith ruby app into Angular
        8 with at least 80% code coverage in unit tests
        and e2e test.`,
      `Built Imagine Language & Literacy Educator Dashboards
        in AngularJS & Angular which involved hundreds
        of modules, components, services, directives, unit
        and e2e tests.`,
      `One year worth of C# work building SSO initial setup
        for School Information Systems.`,
      `Worked in a Scrum environment with constant collaboration
        with Product Owner and Project Manager coupled with
        numerous cross team collaborations`
    ],
    tags: [
      Tag.Angular,
      Tag.AngularJS,
      Tag.Node,
      Tag.TypeScript,
      Tag.WebPack,
      Tag.Ruby,
      Tag.Postgres,
      Tag.AWS,
      Tag.CSharp,
      Tag.Docker,
      Tag.Kubernetes,
      Tag.SCSS,
      Tag.Scrum,
    ]
  },
  {
    title: 'Web Developer',
    company: 'ALTRES',
    location: 'Honolulu, HI',
    start: 'Apr 2014',
    end: 'Oct 2014',
    descriptions: [
      `Built an in-house Potential Recruiting Application written
        in JavaScript jQuery Widgets, Backbone.js, Mustache, Java
        Maven and Tomcat with MariaDB.`,
      `Improved application memory leaks by introducing the use
        of Handlebars.`,
      `Worked as part of a team of 4 using scrum.`
    ],
    tags: [
      Tag.JQuery,
      Tag.JQueryWidgets,
      Tag.JavaScript,
      Tag.Java,
      Tag.MariaDB
    ]
  },
  {
    title: 'Senior Developer',
    company: 'Tapterra',
    location: 'Honolulu, HI',
    start: 'Jun 2013',
    end: 'Apr 2014',
    descriptions: [
      `Built several customization for Umbraco 7 utilizing C#
          and AngularJS.`,
      `Worked on HMSA.COM (MVC 4 .Net) focus on responsive
          design, CSS3 and HTML5`
    ],
    tags: [
      Tag.AngularJS,
      Tag.Umbracco,
      Tag.CSharp,
      Tag.SQL,
      Tag.JQuery
    ]
  },
  {
    title: 'Web Developer',
    company: 'First Insurance Company of Hawaii',
    location: 'Honolulu, HI',
    start: 'Nov 2012',
    end: 'Jun 2013',
    descriptions: [
      `Built customizations to in house proprietary Insurance Policy Management System`,
      `Provided consultation in finding best technologies to replace company software systems`
    ],
    tags: [
      Tag.Velocity,
      Tag.Java
    ]
  },
  {
    title: 'Senior Developer',
    company: 'Anthology Marketing Group',
    location: 'Honolulu, HI',
    start: 'Feb 2012',
    end: 'Nov 2012',
    descriptions: [
      `Built | Maintain | Troubleshoot several Window and
          Linux Servers from the ground up`,
      `Wrote customizations to enhance classic CMS (RedDot)
          ability`,
      `Lead Programmer for several of the agency’s largest
          clients such as Bank of Hawaii, Marriott, Talentwise,
          Farmers Insurance, WaikikiParc.`,
      `Built several projects from ground up utilizing Umbraco
          (.NET, C#), Drupal (PHP) and RedDot (Classic ASP and
          RedDot coding)`,
      `Custom built several CMS with custom Google Maps
          integrations`
    ],
    tags: [
      Tag.CMS,
      Tag.Drupal,
      Tag.Umbracco,
      Tag.DotNet,
      Tag.JavaScript,
      Tag.JQuery,
      Tag.MySQL
    ]
  },

  {
    title: 'Developer',
    company: 'Anthology Marketing Group',
    location: 'Honolulu, HI',
    start: 'Nov 2010',
    end: 'Feb 2012',
    descriptions: [
      `Performed server migration from FreeBSD to RedHat`,
      `Built | Configured backup solution for company client projects`,
      `Built front-end web interfaces utilizing RedDot and Drupal.`,
      `Did many Drupal custom development (module, theming), maintenance`
    ],
    tags: [
      Tag.CMS,
      Tag.Drupal,
      Tag.Umbracco,
      Tag.JavaScript,
      Tag.JQuery,
      Tag.MySQL
    ]
  },

  {
    title: 'Systems Administrator',
    company: 'Brigham Young University Hawaii - Career & Alumni Services',
    location: 'Laie, HI',
    start: 'March 2010',
    end: 'Nov 2010',
    descriptions: [
      `Coordinated integration of Symplicity CSM with PeopleSoft with
          in house PeopleSoft`
    ],
    tags: [
      Tag.CMS,
      Tag.Drupal,
      Tag.JavaScript,
      Tag.JQuery,
      Tag.MySQL
    ]
  },

  {
    title: 'Developer | IT Consultant',
    company: 'Learning Hale',
    location: 'Honolulu, HI',
    start: 'September 2009',
    end: 'Dec 2011',
    descriptions: [
      `Systems Administrator Lead programmer for Human
          Resource System`,
      `Automated computer imaging process for customers`,
      `Integrated custom PHP System with Wordpress customers
          (LearningHale.com)`
    ],
    tags: [
      Tag.PHP,
      Tag.CMS,
      Tag.JavaScript,
      Tag.JQuery,
      Tag.MySQL,
      Tag.Wordpress
    ]
  }
];

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss']
})
export class ExperienceComponent {
  experiences: Experience[] = experiences;
}
