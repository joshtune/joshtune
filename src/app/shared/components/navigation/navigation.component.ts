import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {clone} from 'lodash';
import {NavItem, navigation, NavigationItem} from './navigation';
import {Observable, Subscription} from 'rxjs';
import {distinctUntilChanged} from 'rxjs/operators';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit, OnDestroy {
  navItems: NavItem[] = navigation;
  @Input() ActiveItem: Observable<NavigationItem>;
  subscriptions: Subscription[] = [];

  toggleActive(index: number, item: NavItem): void {
    if (index === null) {
      const nItems = clone(this.navItems);
      nItems.map(ni => {
        ni.active = ni.label === item.label;
        return ni;
      });
      this.navItems = nItems;
    }
  }

  ngOnInit(): void {
    const sub = this.ActiveItem.pipe(
      distinctUntilChanged()
    ).subscribe((activeItem: NavigationItem) => {
      const navItem = this.navItems.filter((item: NavItem) => item.name === activeItem)[0];
      if (navItem) {
        this.toggleActive(null, navItem);
      }
    });

    this.subscriptions.push(sub);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions = [];
  }

}
