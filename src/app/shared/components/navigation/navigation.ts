export interface NavItem {
  name: NavigationItem;
  label: string;
  active: boolean;
  scrollToCssId: string;
  scrollToOffset: number;
}

export enum NavigationItem {
  Home = 'home',
  Profile = 'profile',
  Experience = 'experience',
  Portfolio = 'portfolio',
  Contact = 'contact'
}

export const navigation: NavItem[] = [
  {
    name: NavigationItem.Home,
    label: 'Home',
    active: true,
    scrollToCssId: '#home',
    scrollToOffset: -20
  },
  {
    name: NavigationItem.Profile,
    label: 'Profile',
    active: false,
    scrollToCssId: '#profile',
    scrollToOffset: 0
  },
  {
    name: NavigationItem.Experience,
    label: 'Experience',
    active: false,
    scrollToCssId: '#experience',
    scrollToOffset: 0
  },
  {
    name: NavigationItem.Portfolio,
    label: 'Portfolio',
    active: false,
    scrollToCssId: '#portfolio',
    scrollToOffset: 0
  },
  {
    name: NavigationItem.Contact,
    label: 'Contact',
    active: false,
    scrollToCssId: '#contact',
    scrollToOffset: 0
  }
];
