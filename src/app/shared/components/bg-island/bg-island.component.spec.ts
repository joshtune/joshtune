import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BgIslandComponent } from './bg-island.component';

describe('BgIslandComponent', () => {
  let component: BgIslandComponent;
  let fixture: ComponentFixture<BgIslandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BgIslandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BgIslandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
