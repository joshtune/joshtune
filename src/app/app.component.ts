import {AfterViewInit, Component, ElementRef, HostListener, ViewChild} from '@angular/core';
import {Subject} from 'rxjs';
import {NavigationItem} from './shared/components/navigation/navigation';
import {GoogleAnalyticsService} from './shared/services/google-analytics.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {

  @ViewChild('home', {static: true}) homeElement: ElementRef;
  @ViewChild('profile', {static: true}) profileElement: ElementRef;
  @ViewChild('experience', {static: true}) experienceElement: ElementRef;
  @ViewChild('contact', {static: true}) contactElement: ElementRef;

  activeItem$ = new Subject<NavigationItem>();

  currentElementVisible: NavigationItem;

  elementPositions: {
    home: number;
    profile: number;
    experience: number;
    contact: number;
  } = {
    home: null,
    profile: null,
    experience: null,
    contact: null
  };

  constructor(private googleAnalyticsService: GoogleAnalyticsService) {
  }

  ngAfterViewInit(): void {
    this.elementPositions.home = this.homeElement.nativeElement.offsetTop;
    this.elementPositions.profile = this.profileElement.nativeElement.offsetTop;
    this.elementPositions.experience = this.experienceElement.nativeElement.offsetTop;
    this.elementPositions.contact = this.contactElement.nativeElement.offsetTop;
  }

  @HostListener('window:scroll', ['$event'])
  handleScroll(event): void {
    if (this.elementVisible(this.homeElement)) {
      if (this.currentElementVisible !== NavigationItem.Home) {
        this.currentElementVisible = NavigationItem.Home;
        this.googleAnalyticsService.eventEmitter({
          category: `Scrolling`,
          action: `ScrollTo`,
          label: `${NavigationItem.Home}`,
        });
        this.activeItem$.next(NavigationItem.Home);
      }
    } else if (this.elementVisible(this.profileElement)) {
      if (this.currentElementVisible !== NavigationItem.Profile) {
        this.currentElementVisible = NavigationItem.Profile;
        this.googleAnalyticsService.eventEmitter({
          category: `Scrolling`,
          action: `ScrollTo`,
          label: `${NavigationItem.Profile}`,
        });
        this.activeItem$.next(NavigationItem.Profile);
      }
    } else if (this.elementVisible(this.experienceElement)) {
      if (this.currentElementVisible !== NavigationItem.Experience) {
        this.currentElementVisible = NavigationItem.Experience;
        this.googleAnalyticsService.eventEmitter({
          category: `Scrolling`,
          action: `ScrollTo`,
          label: `${NavigationItem.Experience}`,
        });
        this.activeItem$.next(NavigationItem.Experience);
      }
    } else if (this.elementVisible(this.contactElement)) {
      if (this.currentElementVisible !== NavigationItem.Contact) {
        this.currentElementVisible = NavigationItem.Contact;
        this.googleAnalyticsService.eventEmitter({
          category: `Scrolling`,
          action: `ScrollTo`,
          label: `${NavigationItem.Contact}`,
        });
        this.activeItem$.next(NavigationItem.Contact);
      }
    }
  }

  elementVisible(element: ElementRef): boolean {
    const elementTopPosition = element.nativeElement.offsetTop - 10;
    const elementBottomPosition = (element.nativeElement.offsetHeight + element.nativeElement.offsetTop) - 10;
    return window.scrollY >= elementTopPosition && window.scrollY <= elementBottomPosition;
  }
}
